package jp.co.kenshu.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.kenshu.entity.Comment;

public interface CommentMapper {

	//全件取得(同時にList化)
	List<Comment> getCommentAll();

	//データベースに投稿追加
	// springの時引数が2個の場合は@Paramを付ける
	int insertComment(@Param("text")String text, @Param("message_id")int messageId,
		@Param("created_date")Date createdDate, @Param("updated_date")Date updatedDate);

	int updatedComment(int messageId);
}
