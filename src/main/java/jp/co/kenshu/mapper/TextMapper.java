package jp.co.kenshu.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.kenshu.entity.Text;

public interface TextMapper {

	// メソッドが呼ばれた際にTextMapper.xmlを利用

	//一件取得
	Text getText(int id);

	//全件取得(同時にList化)
	List<Text> getTextAll();

	//データベースに投稿追加
	int insertText(@Param("text")String text, @Param("createdDate")Date createdDate, @Param("updatedDate")Date updatedDate);
}
