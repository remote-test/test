package jp.co.kenshu.controller;

import java.util.List;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.dto.text.CommentDto;
import jp.co.kenshu.dto.text.TextDto;
import jp.co.kenshu.form.CommentForm;
import jp.co.kenshu.form.TextForm;
import jp.co.kenshu.service.TextService;

@Controller
public class TextController {

	// DIでTestServiceをインスタンス化
	@Autowired
    private TextService textService;

	@RequestMapping(value = "/allMessage/{id}", method = RequestMethod.GET)
    public String text(Model model, @PathVariable int id) {
        TextDto text = textService.getText(id);
        model.addAttribute("text", text);
        //text.jspに入っていく
        return "text";
    }

	// 投稿を全件取得
    // http://localhost:8080/20ch/allMessage(トップ画面)

    @RequestMapping(value = "/allMessage", method = RequestMethod.GET)
    public String textAll(Model model) {
        List<TextDto> texts = textService.getTextAll();
        //このsentenceはtextAll.jspに対応
        model.addAttribute("sentence", "投稿はこちら");
        // TextServiceから取得したTestDtoの集まり(List)を、modelにadd
        model.addAttribute("texts", texts);
        // viewからtextAll.jspを探し、表示

        // // コメントを全件取得
        List<CommentDto> comments = textService.getCommentAll();
        model.addAttribute("comments", comments);

        CommentForm form = new CommentForm();
        model.addAttribute("commentForm", form);

        return "textAll";
    }

    //投稿をデータべースに追加
    //http://localhost:8080/20ch/newMessage(新規投稿画面)

    @RequestMapping(value = "/newMessage", method = RequestMethod.GET)
    public String textInsert(Model model) {
        TextForm form = new TextForm();
        model.addAttribute("textForm", form);
        model.addAttribute("sentence", "投稿しなさいな");
        model.addAttribute("sentence1", "たくさん投稿しなさいな");
        return "textInsert";
    }

    @RequestMapping(value = "/newMessage", method = RequestMethod.POST)
    // 投稿内容がTextFormにセットされた状態でtextInsertメソッドに渡ってくる
    public String textInsert(@ModelAttribute TextForm form, Model model) {
        int count = textService.insertText(form.getText(), form.getCreatedDate(), form.getUpdatedDate());
        Logger.getLogger(TextController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
        // トップ画面に遷移
        return "redirect:/allMessage";
    }

    @RequestMapping(value = "/allMessage", method = RequestMethod.POST)
    // 投稿内容がTextFormにセットされた状態でtextInsertメソッドに渡ってくる
    public String textInsert(@ModelAttribute CommentForm form, Model model) {
        int count02 = textService.insertComment(form.getText(), form.getMessageId(), form.getCreatedDate(), form.getUpdatedDate());
        int count03 = textService.updatedComment(form.getMessageId());
        Logger.getLogger(TextController.class).log(Level.INFO, "挿入件数は" + count02 + "件です。");
        Logger.getLogger(TextController.class).log(Level.INFO, "挿入件数は" + count03 + "件です。");
        // トップ画面に遷移
        return "redirect:/allMessage";
    }
}