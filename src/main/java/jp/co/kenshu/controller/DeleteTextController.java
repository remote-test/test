package jp.co.kenshu.controller;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.form.TextForm;
import jp.co.kenshu.service.DeleteTextService;

@Controller
public class DeleteTextController {

	@Autowired
    private DeleteTextService deleteTextService;

	@RequestMapping(value = "/allMessage", method = RequestMethod.POST)
    public String textDelete(@ModelAttribute TextForm form, Model model) {
        int count04 = deleteTextService.deleteText(form.getIsStopped());
        Logger.getLogger(DeleteTextController.class).log(Level.INFO, "挿入件数は" + count04 + "件です。");
        // トップ画面に遷移
        return "redirect:/allMessage";
    }
}
