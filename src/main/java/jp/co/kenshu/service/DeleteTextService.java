package jp.co.kenshu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.kenshu.mapper.DeleteTextMapper;

@Service
public class DeleteTextService {

	@Autowired
    private DeleteTextMapper deleteTextMapper;

	public int deleteText(int isStopped) {
        int count04 = deleteTextMapper.deleteText(isStopped);
        return count04;
    }
}
