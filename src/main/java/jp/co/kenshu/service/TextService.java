package jp.co.kenshu.service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.kenshu.dto.text.CommentDto;
import jp.co.kenshu.dto.text.TextDto;
import jp.co.kenshu.entity.Comment;
import jp.co.kenshu.entity.Text;
import jp.co.kenshu.mapper.CommentMapper;
import jp.co.kenshu.mapper.TextMapper;

@Service
public class TextService {

	// DIでTextMapperをインスタンス化
	@Autowired
    private TextMapper textMapper;

	//一件取得
    public TextDto getText(Integer id) {
        TextDto dto = new TextDto();
        Text entity = textMapper.getText(id);
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    //全件取得
    // mapperから取得した結果をListに詰める
    public List<TextDto> getTextAll() {
        List<Text> textList = textMapper.getTextAll();
        List<TextDto> resultList = convertToDto(textList);
        return resultList;
    }

    // Listに詰められたentityをDtoに詰めなおす
    private List<TextDto> convertToDto(List<Text> textList) {
        List<TextDto> resultList = new LinkedList<>();
        for (Text entity : textList) {
            TextDto dto = new TextDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }

    // データベースに投稿追加
    // MapperのinsertTextメソッドにtextを渡し、戻り値をreturn
    public int insertText(String text, Date createdDate, Date updatedDate) {
        int count = textMapper.insertText(text, createdDate, updatedDate);
        return count;
    }

    @Autowired
    private CommentMapper commentMapper;

  //全件取得
    // mapperから取得した結果をListに詰める
    public List<CommentDto> getCommentAll() {
        List<Comment> commentList = commentMapper.getCommentAll();
        List<CommentDto> resultList = convertToCommentDto(commentList);
        return resultList;
    }

    // Listに詰められたentityをDtoに詰めなおす
    private List<CommentDto> convertToCommentDto(List<Comment> commentList) {
        List<CommentDto> resultList = new LinkedList<>();
        for (Comment entity : commentList) {
            CommentDto dto = new CommentDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }

    // データベースにコメント追加
    // MapperのinsertCommentメソッドにtextを渡し、戻り値をreturn
    public int insertComment(String text, int messageId, Date createdDate, Date updatedDate) {
        int count02 = commentMapper.insertComment(text, messageId, createdDate, updatedDate);
        return count02;
    }

    public int updatedComment(int messageId) {
        int count03 = commentMapper.updatedComment(messageId);
        return count03;
    }
}
