<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
    <head>
        <meta charset="utf-8">
        <title>トップ</title>
    </head>
    <body>
    	<c:url value="/newMessage" var="newMessageUrl" />
		<a href="${newMessageUrl}">新規投稿</a>

    	<!-- このsentenceはTextControllerと対応 -->
        <h1>${sentence}</h1>

		<!-- TextControllerから渡ったtexts -->
        <c:forEach items="${texts}" var="text">
        	<!-- 投稿の表示 -->
        	<p><c:out value="${text.text}"></c:out></p>

        	<!-- 投稿削除機能 -->
        	<form:form modelAttribute="commentForm" action="allMessage">
            	<input type="submit" value="削除">
            </form:form>

            <!-- コメント表示 -->
            <c:forEach items="${comments}" var="comment">
        		<c:if test="${text.id == comment.messageId}">
            		<p><c:out value="${comment.text}"></c:out></p>
            		<!-- コメント削除機能 -->
            		<input type="submit" value="削除">
            	</c:if>
        	</c:forEach>

        	<!-- コメントフォーム -->
        	<form:form modelAttribute="commentForm" action="allMessage">
		    	<form:hidden path="messageId" value="${text.id}"/>
		        <form:textarea path="text" cols="70" rows="2"/>
		        <input type="submit" value="投稿">
    		</form:form>
        </c:forEach>
    </body>
</html>