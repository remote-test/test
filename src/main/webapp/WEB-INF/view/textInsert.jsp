<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<meta charset="utf-8">
<title>新規投稿</title>
</head>
<body>

	<c:url value="/allMessage" var="allMessageUrl" />
	<a href="${allMessageUrl}">トップ</a>

	<!-- 投稿をデータベースに追加 -->
    <h1>${sentence}</h1>
    <h2>${sentence1}</h2>
    <form:form modelAttribute="textForm">
    <!-- setter,getterのmessage -->
        <form:textarea path="text" cols="100" rows="5"/>
        <input type="submit" value="投稿">
    </form:form>
</body>
</html>